import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Parser {

    public String getHTML(String readUrl) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String result = "";
        try {
            url = new URL(readUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = rd.readLine();
            rd.close();
        } catch (Exception e) {
            e.getMessage();
        }
        return result;
    }
}